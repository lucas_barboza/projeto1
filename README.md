# pacote1

> Pacote de exemplo

## Instalação
> Requer PHP 7

```bash
composer require lgbarboza/pacote1
```

```php
namespace lgbarboza\pacote1;
class Exemplo {
    /**
     *  Retorna o meu nome
     */
    public function meuNomCompleto();
}
```

## Licença
MIT