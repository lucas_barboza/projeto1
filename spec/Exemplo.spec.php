<?php
    use LucasBarboza\Pacote1\Exemplo;

    describe("Exemplo", function() {
        describe("nome", function() {
            it("contem Lucas", function() {
                $e = new Exemplo();
                expect($e->meuNomCompleto())->toContain('Lucas');
            });
        });
    });